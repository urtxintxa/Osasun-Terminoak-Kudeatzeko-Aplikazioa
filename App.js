import React, { Component } from 'react';
import { Root } from "native-base";
import { Provider } from 'react-redux';

import store from './src/store/store';

import { isSignedIn } from './src/config/auth';

import { createRootNavigator } from './src/navigators/MyNavigator'

export default class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      signedIn: false,
      checkedSignIn: false
    };
  }

  componentDidMount() {
    isSignedIn()
      .then(res => this.setState({ signedIn: res, checkedSignIn: true }))
      .catch(err => alert("An error occurred"));
  }

  render() {

    const { checkedSignIn, signedIn } = this.state;

    if (!checkedSignIn) {
      return null;
    }

    const Layout = createRootNavigator(signedIn);

    return (
      <Provider store={store}>
        <Root>
          <Layout />
        </Root>
      </Provider>
    );
  }
}
