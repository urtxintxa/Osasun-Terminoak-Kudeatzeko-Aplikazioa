import * as actionTypes from './actionTypes';
import Data from '../data.json';
import {url} from '../config/api';


export function cancelQuery() {
  return (dispatch) => {
    dispatch({ type: DATA_CANCEL });
  }
}

/*
  Functions for Searching Terms
*/

export function getData(value) {
  return (dispatch) => {
    if (value.length > 2) {
      const actionId = Math.random();
      dispatch({ type: actionTypes.DATA_LOAD, id: actionId });

      return fetchTerms(value).then(([response, json]) => {
        if (response.status === 200 && Object.keys(json.matches).length > 0) {
          dispatch(fetchSuccess(json, actionId));
        }
        else if (!response.status) {
          dispatch({ type: actionTypes.DATA_NO_INTERNET });
        }
        else {
          dispatch({ type: actionTypes.DATA_ERROR });
        }
      })
    }
    else {
      return dispatch({ type: actionTypes.DATA_INITIAL });
    }
  };
}

function fetchTerms(value) {
  var URL = `${url}descriptions?query=${value}&mode=partialMatching&returnLimit=20`;
  return fetch(URL, { method: 'GET' })
    .then(response => Promise.all([response, response.json()]))
    .catch(error => Promise.all([error, []]));
}

function fetchSuccess(payload, actionId) {
  return {
    type: actionTypes.DATA_AVAILABLE,
    terms: payload.matches,
    id: actionId
  }
}

export function clearTerm() {
  return (dispatch) => {
    dispatch({ type: actionTypes.CLEAR_TERM });
  }
}

/*
  Functions for getting term info
*/

function fetchTerm(value, type) {

  switch (type) {
    case 1:
      var URL = `${url}concepts/${value}/parents?form=inferred`;
      break;
    case 2:
      var URL = `${url}concepts/${value}/children?form=inferred`;
      break;
    default:
      var URL = `${url}concepts/${value}/`;
  }

  return fetch(URL, {
    method: 'GET',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
  })
    .then(response => Promise.all([response, response.json()]))
    .catch((error) => {
      console.error(error);
    });
}

export function getTerm(value, type) {
  return (dispatch) => {
    return fetchTerm(value, type).then(([response, json]) => {
      if (response.status === 200) {
        dispatch(fetchTermSuccess(json, type));
      }
      else {
        dispatch(fetchError());
      }
    })
  };
}

function fetchTermSuccess(payload, type) {

  switch (type) {
    case 1:
      return {
        type: actionTypes.PARENTS_AVAILABLE,
        parents: payload
      }
      break;
    case 2:
      return {
        type: actionTypes.CHILDREN_AVAILABLE,
        children: payload
      }
      break;
    default:
      return {
        type: actionTypes.TERM_AVAILABLE,
        term: payload
      }
  }
}