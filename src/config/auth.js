import { AsyncStorage } from "react-native";

export const onSignIn = (email, eId) => AsyncStorage.setItem('USER', JSON.stringify({email: email, eId: eId}));

export const onSignOut = () => AsyncStorage.removeItem('USER');

export const isSignedIn = () => {
  return new Promise((resolve, reject) => {
    AsyncStorage.getItem('USER')
      .then(res => {
        if (res !== null) {
          resolve(true);
        } else {
          resolve(false);
        }
      })
      .catch(err => reject(err));
  });
};