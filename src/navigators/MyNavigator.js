import React from 'react';
import { StackNavigator, DrawerNavigator, SwitchNavigator } from 'react-navigation'

// Screens
import SearchScreen from '../components/SearchScreen'
// import FavScreen from '../components/FavScreen'
import InfoScreen from '../components/InfoScreen'
import TermScreen from '../components/TermScreen'
import LoginScreen from '../components/LoginScreen'
import RegisterScreen from '../components/RegisterScreen'
import CorrectScreen from '../components/CorrectScreen'

import Drawer from '../components/Drawer'

export const Home = StackNavigator({
  SearchScreen: {
    screen: SearchScreen
  },
  TermScreen: {
    screen: TermScreen
  },
  CorrectScreen: {
    screen: CorrectScreen
  }
});
/*
export const Fav = StackNavigator({
  FavScreen: {
    screen: FavScreen
  }
});
*/

export const Info = StackNavigator({
  InfoScreen: {
    screen: InfoScreen
  }
});

export const Login = StackNavigator({
  LoginScreen: {
    screen: LoginScreen
  },
  RegisterScreen: {
    screen: RegisterScreen
  }
});

export const MyNavigator = DrawerNavigator(
  {
    Home: {
      screen: Home
    },
    /*
    Fav: {
      screen: Fav
    },
    */
    Info: {
      screen: Info
    }
  },
  {
    contentComponent: props => <Drawer {...props} />
  }

);

export const createRootNavigator = (signedIn = false) => {

  return SwitchNavigator(
    {
      Login: {
        screen: Login
      },
      MyNavigator: {
        screen: MyNavigator
      }
    },
    {
      initialRouteName: signedIn ? "MyNavigator" : "Login"
    }
  );

};
