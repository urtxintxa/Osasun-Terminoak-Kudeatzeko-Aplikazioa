const styles = {
  term: {
    padding: 5,
    paddingLeft: 15,
    paddingRight: 15,
    backgroundColor: '#FFFFFF'
  },
  header: {
    fontWeight: 'bold',
    color: '#404040'
  },
  fsn: {
    color: '#b6b6b6'
  }
};

export default styles;
