import React, { PropTypes } from 'react';
import {
  Text,
  View } from 'react-native';

import styles from './style'

const TermListItem = ({header, fsn}) => (
  <View style={styles.term}>
    <Text style={styles.header}>{header}</Text>
    {fsn && fsn.length > 0 &&
      <Text>{fsn}</Text>
    }
  </View>
);

export default TermListItem
