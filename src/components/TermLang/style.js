const styles = {
  term: {
    flexDirection: 'row',
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 15,
    paddingRight: 15,
    backgroundColor: '#FFFFFF'
  },
  lang: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingRight: 15
  },
  termName: {
    fontWeight: 'bold',
    color: '#404040'
  }
};

export default styles;
