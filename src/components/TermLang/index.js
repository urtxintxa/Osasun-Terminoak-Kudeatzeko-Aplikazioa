import React from 'react';
import {
  Text,
  View
} from 'react-native';

import styles from './style'

const TermLang = ({ lang, term, fsn }) => (
  <View style={styles.term}>
    <View style={styles.lang}>
      {lang === 'EU' && <Text style={{ fontWeight: 'bold', color: '#e6c331' }}>{lang}</Text>}
      {lang === 'ES' && <Text style={{ fontWeight: 'bold', color: '#2196f3' }}>{lang}</Text>}
      {lang === 'EN' && <Text style={{ fontWeight: 'bold', color: '#795545' }}>{lang}</Text>}
    </View>
    <View>
      <Text style={styles.termName}>{term}</Text>
      {fsn && fsn.length > 0 &&
        <Text>{fsn}</Text>
      }
    </View>
  </View>
);

export default TermLang;
