import React, { Component } from 'react';
import { ScrollView, Text } from 'react-native'
import { DrawerItems, SafeAreaView } from 'react-navigation'
import { Icon, Button } from 'native-base'
import { onSignOut } from '../../config/auth';

import styles from './style';

export default class Drawer extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <ScrollView>
        <SafeAreaView forceInset={{ top: 'always', horizontal: 'never' }} style={{ flex: 1 }}>
          <DrawerItems {...this.props} />
          <Button
            small
            transparent
            onPress={() => (onSignOut().then(() => this.props.navigation.navigate('Login')))}
          >
            <Icon
              type='MaterialCommunityIcons'
              name='logout'
              style={{ color: 'black', fontSize: 26 }}
            />
            <Text>Saioa itxi</Text>
          </Button>

        </SafeAreaView>
      </ScrollView>
    )
  }
}
