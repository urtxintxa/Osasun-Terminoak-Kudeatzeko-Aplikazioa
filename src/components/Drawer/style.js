const styles = {
  header: {
    backgroundColor: 'black',
    height: 150,
    flex: 1
  },
  headerLogo: {
    resizeMode: 'contain',
    width: 250
  },
}

export default styles;
