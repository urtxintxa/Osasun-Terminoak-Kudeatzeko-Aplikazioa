import React, { Component } from 'react';
import { ScrollView, Text, View, FlatList } from 'react-native';
import { Icon } from 'native-base'

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import CardView from '../CardView'
import TermLang from '../TermLang'

import * as Actions from '../../actions';

class TermScreen extends Component {

  constructor(props) {
    super(props);

    this.props.clearTerm();

    const id = this.props.navigation.getParam('id', '0');
    this.props.getTerm(id, 0); // Term
    this.props.getTerm(id, 1); // Parents
    this.props.getTerm(id, 2); // Children

    this.state = {
      favorite: null,
      showToast: false
    };

  }

  _toCorrectScreen() {
    this.props.navigation.navigate('CorrectScreen', { data: this.props.data});
  }

  /*
  _changeLikeIcon() {
    this.checkLike(this.props.data.conceptId, true);
  }
  */

  componentDidMount() {
      // this.checkLike(id, false);

    this.props.navigation.setParams({
      toCorrectScreen: this._toCorrectScreen.bind(this),
      // changeLikeIcon: this._changeLikeIcon.bind(this),
      favorite: this.state.favorite
    });
  }

  /*
  async checkLike(id, change) {
    if (id) {
      try {
        AsyncStorage.getItem(`_${id}`).then((value) => {
          if (value && value !== null) {
            this.setState({ favorite: 'favorite' });
            if (change) {
              AsyncStorage.removeItem(`_${id}`);
              Toast.show({
                text: "Terminoa gogokoen zerrendatik ezabatu da",
                duration: 3000
              });
              this.setState({ favorite: 'favorite-border' });
            }
            this.props.navigation.setParams({
              favorite: this.state.favorite
            });
          }
          else {
            this.setState({ favorite: 'favorite-border' });
            if (change) {
              AsyncStorage.setItem(`_${id}`, id);
              Toast.show({
                text: "Terminoa gogokoen zerrendara gehitu da",
                duration: 3000
              });
              this.setState({ favorite: 'favorite' });
            }
            this.props.navigation.setParams({
              favorite: this.state.favorite
            });
          }
        }).done();
      } catch (error) {
        console.log(error);
      }
    }
  }
  
        <Icon
          type='MaterialIcons'
          name={navigation.state.params.favorite}
          style={{ paddingRight: 16, fontSize: 26, color: 'red' }}
          onPress={navigation.state.params.changeLikeIcon} />
  */

  static navigationOptions = ({ navigation, screenProps }) => ({
    title: 'Terminoa',
    headerLeft:
      <View style={{ paddingLeft: 16 }}>
        <Icon
          type='MaterialIcons'
          name='arrow-back'
          style={{ fontSize: 26 }}
          onPress={() => (navigation.goBack())} />
      </View>,
    headerRight:
      <View style={{ paddingRight: 16, flexDirection: 'row' }}>
        <Icon
          type='MaterialIcons'
          name='edit'
          style={{ fontSize: 26, color: 'black' }}
          onPress={navigation.state.params.toCorrectScreen} />
      </View>
  });

  render() {
    return (
      <ScrollView>
        {this.props.data.module && this.props.data.module === '900000000000207008' &&
          <CardView>
            <TermLang lang='EU' term={this.props.data.descriptions.filter(term => term.lang === 'da').shift().term} />
            <TermLang lang='ES' term={this.props.data.fsn} />
            <TermLang lang='EN' term={this.props.data.descriptions.filter(term => term.lang === 'en' && term.type.defaultTerm === 'descripción completa (metadato del núcleo)').pop().term} />
          </CardView>
        }
        <CardView header='Sinonimoak'>
          <Text style={{ fontWeight: 'bold', color: '#e6c331' }}>Euskara</Text>
          {this.props.data.descriptions &&
            <FlatList
              data={this.props.data.descriptions.filter(term => term.lang === 'da')}
              renderItem={({ item }) => <Text>{item.term}</Text>}
              keyExtractor={(item, index) => item.term}
            />
          }
          <Text style={{ fontWeight: 'bold', color: '#2196f3' }}>Gaztelaina</Text>
          {this.props.data.descriptions &&
            <FlatList
              data={this.props.data.descriptions.filter(term => term.lang === 'es')}
              renderItem={({ item }) => <Text>{item.term}</Text>}
              keyExtractor={(item, index) => item.term}
            />
          }
          <Text style={{ fontWeight: 'bold', color: '#795545' }}>Ingelesa</Text>
          {this.props.data.descriptions &&
            <FlatList
              data={this.props.data.descriptions.filter(term => term.lang === 'en')}
              renderItem={({ item }) => <Text>{item.term}</Text>}
              keyExtractor={(item, index) => item.term}
            />
          }
        </CardView>
        <CardView header='Egitura Hierarkikoa'>
          <Text style={{ fontWeight: 'bold', color: '#404040' }}>TERMINO GURASOAK</Text>
          <FlatList
            data={this.props.parents}
            renderItem={({ item }) => <Text>{item.defaultTerm}</Text>}
            keyExtractor={(item, index) => item.defaultTerm}
          />
          <Text style={{ fontWeight: 'bold', color: '#404040' }}>TERMINO UMEAK</Text>
          <FlatList
            data={this.props.children}
            renderItem={({ item }) => <Text>{item.defaultTerm}</Text>}
            keyExtractor={(item, index) => item.defaultTerm}
          />
        </CardView>
      </ScrollView>
    )
  }
};

function mapStateToProps(state, props) {
  return {
    data: state.termReducer.data,
    parents: state.parentsReducer.data,
    children: state.childrenReducer.data
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}


export default connect(mapStateToProps, mapDispatchToProps)(TermScreen);