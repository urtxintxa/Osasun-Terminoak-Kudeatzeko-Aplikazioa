import { Platform } from 'react-native';

const styles = {
  card: {
    backgroundColor: 'white',
    borderColor: '#e1e8ee',
    borderWidth: 1,
    padding: 15,
    margin: 15,
    marginBottom: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'rgba(0,0,0, .2)',
        shadowOffset: { height: 0, width: 0 },
        shadowOpacity: 1,
        shadowRadius: 1,
      },
      android: {
        elevation: 1,
      },
    }),
  },
  header: {
    textAlign: 'center',
    marginBottom: 15,
    fontWeight: 'bold',
    color: '#393e42'
  },
  separator: {
    height: 1,
    backgroundColor: '#e1e8ee',
    marginBottom: 15
  }
};

export default styles;
