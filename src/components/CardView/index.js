import React from 'react';
import {
  Text,
  View } from 'react-native';

import styles from './style'

const CardView = ({children, header}) => (
  <View style={styles.card}>
    {header && header.length > 0 &&
      <View>
        <Text style={styles.header}>{header}</Text>
        <View style={styles.separator}/>
      </View>
    }
    {children}
  </View>
);

export default CardView;
