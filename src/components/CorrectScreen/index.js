import React, { Component } from 'react';
import { AsyncStorage, ScrollView, Text, TextInput, View, FlatList } from 'react-native';
import { Button, CheckBox, Icon, Radio } from 'native-base';

import CardView from '../CardView';
import TermLang from '../TermLang';

import { url1 } from '../../config/api';

export default class CorrectScreen extends Component {

  constructor(props) {
    super(props);

    this.state = {
      data: null,
      eu: null,
      selectedId: [],
      selectedIdFSN: 0,
      editedTerms: [],
      newTerms: [],
      user: null
    };
  }

  componentDidMount() {
    this.setState({ data: this.props.navigation.getParam('data', '0') }, function () {
      this.setState({ eu: this.state.data.descriptions.filter(term => term.lang === 'da') })
    });

    this.onRadioPress(0);

    AsyncStorage.getItem("USER").then((value) => {
      this.setState({ user: JSON.parse(value).eId });
    });
  }

  onCheckPress(index) {

    aux = this.state.selectedId;

    if (aux.length == 0 || !aux.includes(index)) {
      aux.push(index);

      this.setState({
        selectedId: aux
      });
    }
    else {
      aux.splice(aux.indexOf(index), 1);

      this.setState({
        selectedId: aux
      });
    }
  }

  onRadioPress(index) {

    aux = this.state.selectedId;

    if (aux.length == 0 || !aux.includes(index)) {
      aux.push(index);

      this.setState({
        selectedId: aux
      });
    }

    this.setState({
      selectedIdFSN: index
    })

  }

  onEditPress(index, b) {
    aux = this.state.editedTerms;

    if (b) {
      aux.push({ index: index, term: '' });
    }
    else {
      aux.splice(aux.indexOf(aux.find(elem => elem.index == index)), 1);
    }

    this.setState({
      editedTerms: aux
    });
  }

  newTerm() {
    aux = this.state.newTerms;

    aux.push({ term: '' });

    this.setState({
      newTerm: aux
    });

  }

  changeEditedValue(index, term) {
    aux = this.state.editedTerms;

    aux.filter(elem => elem.index == index).forEach(function (elem) { elem.term = term; })

    this.setState({
      editedTerms: aux
    });
  }

  changeNewTermValue(index, term) {
    aux = this.state.newTerms;

    aux[index].term = term;

    this.setState({
      newTerm: aux
    });
  }

  deleteNew(index) {
    aux = this.state.newTerms;

    aux.splice(index, 1);

    this.setState({
      newTerm: aux
    });

    var newIndex = index + this.state.eu.length

    this.onCheckPress(newIndex); // Delete from checked list

    if (this.state.selectedIdFSN == (newIndex)) {
      this.onRadioPress(0);
    }
    else if (this.state.selectedIdFSN > newIndex) {
      this.onRadioPress(newIndex);
    }

  }

  sendValues() {
    this.state.eu.forEach((element, index) => {

      var selected = this.state.selectedId;
      var edited = this.state.editedTerms;
      var user = this.state.user;
      var selectedIdFSN = this.state.selectedIdFSN;
      var checkAndInsertDescription = this.checkAndInsertDescription;

      if (!selected.includes(index)) {
        this.checkAndInsertDescription(element.term, element.conceptId, user, false, 2); // Term has been ignored
      }
      else if (edited.find(elem => elem.index == index)) {
        this.checkAndInsertDescription(element.term, element.conceptId, user, false, 4) // Term has been ignored because change
          .then(function (data) {
            var elem = edited.find(elem => elem.index == index);
            alert(elem.term);
            checkAndInsertDescription(elem.term, element.conceptId, user, selectedIdFSN == index, data[0].dId);
          });
      }
      else {
        this.checkAndInsertDescription(element.term, element.conceptId, user, selectedIdFSN == index, 1); // Term has been validated
      }
    });

    var conceptId = this.state.eu[0].conceptId;

    this.state.newTerms.forEach((element, index) => {
      this.checkAndInsertDescription(element.term, conceptId, this.state.user, this.state.selectedIdFSN == (index + this.state.eu.length), 3); // New term from user
    });
  }

  checkAndInsertDescription(term, cId, user, hobetsia, egoera) {

    var result = fetch(`${url1}/description`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(
        {
          terminoa: term,
          cId: cId
        }
      ),
    })
      .then(function (response) {
        if (response.status == 404) {
          return fetch(`${url1}/newDescription`, {
            method: 'POST',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify(
              {
                terminoa: term,
                cId: cId
              }
            ),
          });
        }
        return response;
      })
      .then(function (response) {
        return response.json();
      })
      .then(function (data) {
        return fetch(`${url1}/validation`, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(
            {
              dId: data[0].dId,
              eId: user
            }
          ),
        });
      })
      .then(function (response) {
        if (response.status == 404) {
          return Promise.reject('finish');
        }
        else {
          return response.json();
        }
      })
      .then(function (data) {
        return fetch(`${url1}/newValidation`, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(
            {
              dId: data.dId,
              eId: user,
              hobetsia: hobetsia,
              egoera: egoera
            }
          ),
        })
      })
      .then(function (response) {
        return response.json();
      })
      .catch(function (error) {
        // alert(error);
      });

    return result;
  }

  static navigationOptions = ({ navigation }) => ({
    title: 'Terminoa editatu',
    headerLeft:
      <View style={{ paddingLeft: 16 }}>
        <Icon
          name='arrow-back'
          style={{ fontSize: 26 }}
          onPress={() => (navigation.goBack())} />
      </View>
  });

  render() {
    return (
      <ScrollView>
        {this.state.data &&
          <CardView>
            <TermLang lang='EN' term={this.state.data.descriptions.filter(term => term.lang === 'en').pop().term} />
            <TermLang lang='ES' term={this.state.data.descriptions.filter(term => term.lang === 'es').map(term => term.term).join('\n')} />
            <View style={{ alignSelf: 'flex-end' }}>
              <Button small>
                <Icon
                  type='MaterialIcons'
                  name='info'
                  style={{ fontSize: 20 }} />
              </Button>
            </View>
          </CardView>
        }
        <CardView>
          <View style={{ alignSelf: 'center' }}>
            <Text style={{ fontWeight: 'bold' }}>TERMINOA EUSKARAZ</Text>
          </View>

        </CardView>
        {this.state.eu &&
          <FlatList
            data={this.state.eu.map(term => term.term)}
            renderItem={({ item, index }) =>
              <CardView>
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ justifyContent: 'center', paddingRight: 10 }}>
                    <CheckBox
                      color='green'
                      checked={this.state.selectedIdFSN == index}
                      onPress={() => this.onRadioPress(index)}
                    />
                  </View>
                  <View style={{ justifyContent: 'center' }}>
                    <CheckBox
                      checked={this.state.selectedId.length != 0 && this.state.selectedId.includes(index)}
                      onPress={() => this.onCheckPress(index)}
                    />
                  </View>
                  <View style={{ justifyContent: 'center' }}>
                    <Text
                      style={{ paddingLeft: 20 }}
                    >{item}</Text>
                  </View>
                </View>
                {!this.state.editedTerms.find(elem => elem.index == index) &&
                  <Button
                    small
                    transparent
                    style={{ alignSelf: 'flex-end' }}
                    onPress={() => this.onEditPress(index, true)}
                  >
                    <Text>Editatu</Text>
                  </Button>
                }
                {this.state.editedTerms.find(elem => elem.index == index) &&
                  <View>
                    <TextInput
                      underlineColorAndroid='transparent'
                      style={{ borderBottomWidth: 1 }}
                      value={this.state.editedTerms.find(elem => elem.index == index).term}
                      onChangeText={(text) => this.changeEditedValue(index, text)}
                    />
                    <Button
                      small
                      transparent
                      style={{ alignSelf: 'flex-end' }}
                      onPress={() => this.onEditPress(index, false)}
                    >
                      <Text>Ezeztatu</Text>
                    </Button>
                  </View>
                }
              </CardView>
            }
            keyExtractor={(item, index) => index.toString()}
          />
        }
        {this.state.newTerms && this.state.eu &&
          <FlatList
            data={this.state.newTerms.map(term => term.term)}
            renderItem={({ item, index }) =>
              <CardView>
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ justifyContent: 'center', paddingRight: 10 }}>
                    <CheckBox
                      color='green'
                      checked={this.state.selectedIdFSN == (index + this.state.eu.length)}
                      onPress={() => this.onRadioPress(index + this.state.eu.length)}
                    />
                  </View>
                  <View style={{ justifyContent: 'center' }}>
                    <CheckBox
                      checked
                    />
                  </View>
                  <View style={{ paddingLeft: 15, flex: 1 }}>
                    <TextInput
                      underlineColorAndroid='transparent'
                      style={{ borderBottomWidth: 1 }}
                      value={this.state.newTerms[index].term}
                      onChangeText={(text) => this.changeNewTermValue(index, text)}
                    />
                  </View>
                </View>
                <Button
                  small
                  transparent
                  style={{ alignSelf: 'flex-end' }}
                  onPress={() => this.deleteNew(index)}
                >
                  <Text>Ezabatu</Text>
                </Button>
              </CardView>
            }
            keyExtractor={(item, index) => index.toString()}
          />
        }
        <Button
          small
          transparent
          style={{ alignSelf: 'center' }}
          onPress={() => this.newTerm()}
        >
          <Text>Gehitu termino berri bat</Text>
        </Button>
        <Button
          small
          style={{ alignSelf: 'flex-end' }}
          onPress={() => this.sendValues()}
        >
          <Icon
            type='MaterialIcons'
            name='send'
            style={{ fontSize: 20 }} />
        </Button>

      </ScrollView>
    )
  }
}