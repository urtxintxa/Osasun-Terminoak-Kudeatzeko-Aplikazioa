'use strict';

import React, { Component } from 'react';
import {
  ActivityIndicator,
  ListView,
  Text,
  TextInput,
  TouchableHighlight,
  View,
} from 'react-native';
import { Icon } from 'native-base';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as Actions from '../../actions';

import TermLang from '../TermLang'

class SearchScreen extends Component {

  constructor(props) {
    super(props);

    this.ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
    this.state = {
      dataSource: this.ds.cloneWithRows(this.props.data),
      termToSearch: ''
    };
  }

  componentDidMount() {
    this.props.getData(this.state.termToSearch);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      dataSource: this.ds.cloneWithRows(nextProps.data)
    })
  }

  static navigationOptions = ({ navigation }) => ({
    title: 'Terminoak Bilatu',
    drawerIcon:
      <Icon
        type='MaterialIcons'
        name='search'
        style={{ fontSize: 26 }} />,
    headerLeft:
      <View style={{ paddingLeft: 16 }}>
        <Icon
          type='MaterialIcons'
          name='menu'
          style={{ fontSize: 26 }}
          onPress={() => navigation.navigate('DrawerOpen')} />
      </View>
  });

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: '100%',
          backgroundColor: '#e1e8ee'
        }}
      />
    );
  };

  handPressElement(id) {
    this.textInput.blur();
    this.props.navigation.navigate('TermScreen', { id: id });
  }

  getLang(module) {
    if (module === '450829007')
      return 'ES'
    else if (module === '554471000005108')
      return 'EU'
    else
      return 'EN'
  }

  submitQuery(term) {
    if (term.length > 2) {
      this.props.getData(term);
    }
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: 'white' }}>
        <View style={{ paddingLeft: 10, paddingRight: 10, backgroundColor: 'white', flexDirection: 'row' }}>
          <View style={{ justifyContent: 'center' }}>
            <Icon
              type='MaterialIcons'
              name='search'
              style={{ fontSize: 32 }} />
          </View>
          <View style={{ height: 40, flex: 1, justifyContent: 'center' }}>
            <TextInput
              style={{ textAlignVertical: "top", marginTop: 0 }}
              ref={input => { this.textInput = input }}
              placeholder="Idatzi terminoa"
              underlineColorAndroid="transparent"
              onChangeText={(termToSearch) => this.submitQuery(termToSearch)} />
          </View>
          <View style={{ justifyContent: 'center' }}>
            <Icon
              type='MaterialIcons'
              name='close'
              style={{ fontSize: 32 }}
              onPress={() => this.textInput.clear()} />
          </View>
        </View>
        {this.renderSeparator()}
        {this.props.state == 0 &&
          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Icon type='MaterialIcons' name='search' style={{ fontSize: 100, color: '#757575' }} />
            <Text>Idatzi bilatu nahi duzun terminoa.{'\n'}Gutxienez 3 karaktere sartu behar dira.</Text>
          </View>
        }
        {this.props.state == 1 &&
          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Icon type='MaterialIcons' name='error' style={{ fontSize: 100, color: '#757575' }} />
            <Text>Ez da terminoa topatu.</Text>
          </View>
        }
        {this.props.state == 4 &&
          <ActivityIndicator
            size='large'
            color="#0000ff"
          />
        }
        {this.props.state == 2 &&
          <ListView
            keyboardShouldPersistTaps='always'
            enableEmptySections={true}
            renderSeparator={this.renderSeparator}
            dataSource={this.state.dataSource}
            renderRow={this.renderRow.bind(this)} />
        }
        {this.props.state == 3 &&
          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Icon type='MaterialIcons' name='cloud-off' style={{ fontSize: 100, color: '#757575' }} />
            <Text>Ez dago internet konexiorik.</Text>
          </View>
        }
      </View>
    );
  }

  renderRow(rowData, sectionID, rowID) {
    return (
      <TouchableHighlight
        activeOpacity={0.3}
        underlayColor={'red'}
        onPress={() => this.handPressElement(rowData.conceptId)}>
        <TermLang
          lang={this.getLang(rowData.module)}
          term={rowData.term}
          fsn={rowData.fsn}
        />
      </TouchableHighlight>
    )
  }
};

function mapStateToProps(state, props) {
  return {
    data: state.termsReducer.data,
    state: state.termsReducer.state
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchScreen);