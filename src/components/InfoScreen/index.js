import React, { Component } from 'react';
import { ScrollView, Text, View } from 'react-native';
import { Icon } from 'react-native-elements';

import CardView from '../CardView'

export default class InfoScreen extends Component {

  static navigationOptions = ({ navigation }) => ({
    title: 'Honi buruz',
    drawerIcon: <Icon name='info' />,
    headerLeft:
      <View style={{ paddingLeft: 16 }}>
        <Icon
          name='menu'
          onPress={() => navigation.navigate('DrawerOpen')} />
      </View>
  });

  render() {
    return (
      <ScrollView>
        <CardView header='Erabilitako liburutegiak'>
          <Text style={{fontWeight: 'bold', color: '#404040'}}>
            react-native-elements
          </Text>
          <Text>
            Lizentzia: MIT
          </Text>
        </CardView>
      </ScrollView>
    )
  }
}
