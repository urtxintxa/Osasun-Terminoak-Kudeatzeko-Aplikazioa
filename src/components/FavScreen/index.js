import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { Icon } from 'react-native-elements';

export default class FavScreen extends Component {

  static navigationOptions = ({ navigation }) => ({
    title: 'Gogoko Terminoak',
    drawerIcon: <Icon name='favorite' />,
    headerLeft:
      <View style={{ paddingLeft: 16 }}>
        <Icon
          name='menu'
          onPress={() => navigation.navigate('DrawerOpen')} />
      </View>
  });

  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Icon name='favorite' size={100} color='#757575' />
        <Text>Gogoko terminoen zerrenda hutsik dago.</Text>
      </View>
    )
  }
}
