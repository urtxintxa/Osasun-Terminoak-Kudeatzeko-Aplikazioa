import React, { Component } from 'react';
import { ScrollView, Text, TextInput, View } from 'react-native';
import { Icon } from 'react-native-elements';
import { Button, Toast } from 'native-base'

import { url1 } from '../../config/api';
import { onSignIn } from '../../config/auth';

export default class LoginScreen extends Component {

  constructor(props) {
    super(props);

    this.state = {
      email: null,
      pass: null
    };
  }

  login() {
    fetch(`${url1}/user`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(
        {
          email: this.state.email,
          pass: this.state.pass,
        }
      ),
    })
      .then((response) => {
        if (response.status == 200) {
          return response.json();
        }
        else {
          Toast.show({
            text: 'Datuak gaizki sartu dira',
            buttonText: 'Ados'
          });
          return Promise.reject('finish');
        }
      })
      .then((data) => {
        onSignIn(this.state.email, data[0].eId).then(() => this.props.navigation.navigate('MyNavigator'));
      })
      .catch(function (error) { });
  }

  render() {
    return (
      <ScrollView
        contentContainerStyle={{ paddingTop: 50 }}>
        <View style={{ padding: 10, paddingLeft: 40, paddingRight: 40 }}>
          <View style={{ backgroundColor: '#DDDDDD', flexDirection: 'row' }}>
            <View style={{ justifyContent: 'center' }}>
              <Icon
                type='MaterialIcons'
                name='mail'
                style={{ fontSize: 32 }} />
            </View>
            <View style={{ height: 40, flex: 1, justifyContent: 'center' }}>
              <TextInput
                style={{ textAlignVertical: "top", marginTop: 0 }}
                ref={input => { this.textInput = input }}
                placeholder='E-mail helbidea'
                onChangeText={(text) => this.setState({ email: text })} />
            </View>
          </View>
        </View>
        <View style={{ padding: 10, paddingLeft: 40, paddingRight: 40 }}>
          <View style={{ backgroundColor: '#DDDDDD', flexDirection: 'row' }}>
            <View style={{ justifyContent: 'center' }}>
              <Icon
                type='MaterialIcons'
                name='lock'
                style={{ fontSize: 32 }} />
            </View>
            <View style={{ height: 40, flex: 1, justifyContent: 'center' }}>
              <TextInput
                style={{ textAlignVertical: "top", marginTop: 0 }}
                ref={input => { this.textInput = input }}
                placeholder='Pasahitza'
                secureTextEntry
                onChangeText={(text) => this.setState({ pass: text })} />
            </View>
          </View>
        </View>
        <View style={{ padding: 10, paddingLeft: 40, paddingRight: 40 }}>
          <Button
            full
            onPress={() => this.login()}
            style={{ backgroundColor: '#DDDDDD' }}
          >
            <Text>SARTU</Text>
          </Button>
          <Button
            transparent
            onPress={() => this.props.navigation.navigate('RegisterScreen')}
          >
            <Text>Kontua sortu</Text>
          </Button>
        </View>
      </ScrollView>
    )
  }
}