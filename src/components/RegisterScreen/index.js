import React, { Component } from 'react';
import { ScrollView, Text, TextInput, View } from 'react-native';
import { Icon } from 'react-native-elements';
import { Button, Toast } from 'native-base'

import { url1 } from '../../config/api';

export default class RegisterScreen extends Component {

  constructor(props) {
    super(props);

    this.state = {
      email: null,
      izena: null,
      abizena: null,
      pass: null,
    };
  }

  register() {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (this.state.email && !reg.test(this.state.email)) {
      Toast.show({
        text: 'Emaila ez da egokia',
        buttonText: 'Ados'
      });
    }
    else if (this.state.pass && this.state.pass.length < 8) {
      Toast.show({
        text: 'Pasahitzak 8 karaktere edo gehiago izan behar ditu',
        buttonText: 'Ados'
      });
    }
    else if (this.state.izena && this.state.abizena) {
      fetch(`${url1}/newUser`, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(
          {
            email: this.state.email,
            izena: this.state.izena,
            abizena: this.state.abizena,
            pass: this.state.pass,
          }
        ),
      })
        .then((response) => response.status)
        .then((responseCode) => {
          if (responseCode == 200) {
            Toast.show({
              text: 'Kontua ondo sortu da',
            });
          }
          else {
            Toast.show({
              text: 'Ezin da zerbitzariarekin konektatu',
            });
          }
        });
    }
    else {
      Toast.show({
        text: 'Ezin dira eremuak hutsik utzi',
      });
    }
  }

  render() {
    return (
      <ScrollView
        contentContainerStyle={{ paddingTop: 50 }}>
        <View style={{ padding: 10, paddingLeft: 40, paddingRight: 40 }}>
          <View style={{ backgroundColor: '#DDDDDD', flexDirection: 'row' }}>
            <View style={{ justifyContent: 'center' }}>
              <Icon
                type='MaterialIcons'
                name='mail'
                style={{ fontSize: 32 }} />
            </View>
            <View style={{ height: 40, flex: 1, justifyContent: 'center' }}>
              <TextInput
                style={{ textAlignVertical: 'top', marginTop: 0 }}
                ref={input => { this.textInput = input }}
                placeholder='E-mail helbidea'
                onChangeText={(text) => this.setState({ email: text })} />
            </View>
          </View>
        </View>
        <View style={{ padding: 10, paddingLeft: 40, paddingRight: 40 }}>
          <View style={{ backgroundColor: '#DDDDDD', flexDirection: 'row' }}>
            <View style={{ justifyContent: 'center' }}>
              <Icon
                type='MaterialIcons'
                name='face'
                style={{ fontSize: 32 }} />
            </View>
            <View style={{ height: 40, flex: 1, justifyContent: 'center' }}>
              <TextInput
                style={{ textAlignVertical: 'top', marginTop: 0 }}
                ref={input => { this.textInput = input }}
                placeholder='Izena'
                onChangeText={(text) => this.setState({ izena: text })} />
            </View>
          </View>
        </View>
        <View style={{ padding: 10, paddingLeft: 40, paddingRight: 40 }}>
          <View style={{ backgroundColor: '#DDDDDD', flexDirection: 'row' }}>
            <View style={{ justifyContent: 'center' }}>
              <Icon
                type='MaterialIcons'
                name='face'
                style={{ fontSize: 32 }} />
            </View>
            <View style={{ height: 40, flex: 1, justifyContent: 'center' }}>
              <TextInput
                style={{ textAlignVertical: 'top', marginTop: 0 }}
                ref={input => { this.textInput = input }}
                placeholder='Abizena'
                onChangeText={(text) => this.setState({ abizena: text })} />
            </View>
          </View>
        </View>
        <View style={{ padding: 10, paddingLeft: 40, paddingRight: 40 }}>
          <View style={{ backgroundColor: '#DDDDDD', flexDirection: 'row' }}>
            <View style={{ justifyContent: 'center' }}>
              <Icon
                type='MaterialIcons'
                name='lock'
                style={{ fontSize: 32 }} />
            </View>
            <View style={{ height: 40, flex: 1, justifyContent: 'center' }}>
              <TextInput
                style={{ textAlignVertical: 'top', marginTop: 0 }}
                ref={input => { this.textInput = input }}
                placeholder='Pasahitza'
                secureTextEntry
                onChangeText={(text) => this.setState({ pass: text })} />
            </View>
          </View>
        </View>
        <View style={{ padding: 10, paddingLeft: 40, paddingRight: 40 }}>
          <Button
            full
            onPress={() => this.register()}
            style={{ backgroundColor: '#DDDDDD' }}
          >
            <Text>SARTU</Text>
          </Button>
          <Button
            transparent
            onPress={() => this.props.navigation.goBack()}
          >
            <Text>Saioa hasi</Text>
          </Button>
        </View>
      </ScrollView>
    )
  }
}