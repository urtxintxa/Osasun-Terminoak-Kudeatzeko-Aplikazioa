import { combineReducers } from 'redux';

import * as actionTypes from '../actions/actionTypes';

let termsState = { data: [], state: 0, ajax: [] };
let termState = { data: [] };

const termsReducer = (state = termsState, action) => {
  switch (action.type) {
    case actionTypes.DATA_LOAD:
      state = Object.assign({}, state, { data: [], state: 4, ajax: state.ajax.concat(action.id) });
      return state;
    case actionTypes.DATA_AVAILABLE:
      if (state.ajax.includes(action.id)) {
        state = Object.assign({}, state, { data: action.terms, state: 2, ajax: [] });
      }
      return state;
    case actionTypes.DATA_CANCEL:
      state = Object.assign({}, state, { data: action.terms, state: 2, ajax: [] });
      return state;
    case actionTypes.DATA_ERROR:
      state = Object.assign({}, state, { data: [], state: 1, ajax: [] });
      return state;
    case actionTypes.DATA_INITIAL:
      state = Object.assign({}, state, { data: [], state: 0, ajax: [] });
      return state;
    case actionTypes.DATA_NO_INTERNET:
      state = Object.assign({}, state, { data: [], state: 3, ajax: [] });
      return state;
    default:
      return state;
  }
};

const termReducer = (state = termState, action) => {
  switch (action.type) {
    case actionTypes.TERM_AVAILABLE:
      state = Object.assign({}, state, { data: action.term });
      return state;
    case actionTypes.CLEAR_TERM:
      state = Object.assign({}, state, { data: [] });
      return state;
    default:
      return state;
  }
};

const parentsReducer = (state = termState, action) => {
  switch (action.type) {
    case actionTypes.PARENTS_AVAILABLE:
      state = Object.assign({}, state, { data: action.parents });
      return state;
    case actionTypes.CLEAR_PARENTS:
      state = Object.assign({}, state, { data: [] });
      return state;
    default:
      return state;
  }
};

const childrenReducer = (state = termState, action) => {
  switch (action.type) {
    case actionTypes.CHILDREN_AVAILABLE:
      state = Object.assign({}, state, { data: action.children });
      return state;
    case actionTypes.CLEAR_CHILDREN:
      state = Object.assign({}, state, { data: [] });
      return state;
    default:
      return state;
  }
};

const rootReducer = combineReducers({
  termsReducer,
  termReducer,
  parentsReducer,
  childrenReducer
});

export default rootReducer;