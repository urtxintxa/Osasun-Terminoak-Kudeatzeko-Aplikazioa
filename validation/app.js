var createError = require('http-errors');
var express = require('express');
var bodyParser = require('body-parser');

var userRouter = require('./routes/user');
var newUserRouter = require('./routes/newUser');

var descriptionRouter = require('./routes/description');
var newDescriptionRouter = require('./routes/newDescription');

var newValidationRouter = require('./routes/newValidation');


var app = express();

app.use(bodyParser.urlencoded({
   extended: false
}));
app.use(bodyParser.json());

app.use('/user', userRouter);
app.use('/newUser', newUserRouter);

app.use('/description', descriptionRouter);
app.use('/newDescription', newDescriptionRouter);

app.use('/newValidation', newValidationRouter);

module.exports = app;
