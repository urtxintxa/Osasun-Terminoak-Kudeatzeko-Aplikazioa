var express = require('express');
var db = require('../db');

var router = express.Router();

router.post('/', function (req, res) {
    var cId = req.body.cId;

    sql = 'SELECT * FROM Deskribapena WHERE cId=?';

    db.query(sql, [cId], function (error, results, fields) {
        if (error) throw error;

        if (results && results.length) {
            res.send(results);
        }
        else {
            res.sendStatus(404);
        }
    });
});

module.exports = router;
