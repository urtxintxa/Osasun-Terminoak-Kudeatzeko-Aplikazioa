var express = require('express');
var db = require('../db');
var sha512 = require('js-sha512').sha512;

var router = express.Router();

router.post('/', function (req, res) {
    var email = req.body.email;
    var pass = req.body.pass;

    sql = 'SELECT * FROM Erabiltzailea WHERE email=? AND pass=?';

    db.query(sql, [email, sha512(pass)], function (error, results, fields) {
        if (error) throw error;

        if (results && results.length) {
            res.sendStatus(200);
        }
        else {
            res.sendStatus(404);
        }
    });
});

module.exports = router;
