var express = require('express');
var db = require('../db');

var router = express.Router();

router.post('/', function (req, res) {
    var dId = req.body.dId;
    var eId = req.body.eId;
    var data = new Date();
    var hobetsia = req.body.hobetsia;
    var egoera = req.body.egoera;

    sql = 'SELECT * FROM Balidazioa WHERE dId=? AND eId=?';

    db.query(sql, [dId, eId], function (error, results, fields) {
        if (results && results.length) {
            sql = 'INSERT INTO Balidazioa VALUES(?,?,?,?,?)';
            db.query(sql, [dId, eId, data, hobetsia, egoera], function (error, results, fields) {
                if (error) res.sendStatus(400);
                res.sendStatus(200);
            });
        }
        else {
            sql = 'UPDATE Balidazioa SET data=?, hobetsia=?, egoera=? WHERE dId=? AND eId=?';
            db.query(sql, [data, hobetsia, egoera, dId, eId], function (error, results, fields) {
                if (error) res.sendStatus(400);
                res.sendStatus(200);
            });
        }
    });
});

module.exports = router;
