var express = require('express');
var db = require('../db');
var sha512 = require('js-sha512').sha512;

var router = express.Router();

router.post('/', function (req, res) {
    var email = req.body.email;
    var izena = req.body.izena;
    var abizena = req.body.abizena;
    var pass = req.body.pass;

    sql = 'INSERT INTO Erabiltzailea(email, izena, abizena, pass) VALUES (?, ?, ?, ?)';

    db.query(sql, [email, izena, abizena, sha512(pass)], function (error, results, fields) {
        if (error) throw error;
        res.send(JSON.stringify(results));
    });
});

module.exports = router;
