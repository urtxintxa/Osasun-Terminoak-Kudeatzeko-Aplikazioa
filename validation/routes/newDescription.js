var express = require('express');
var db = require('../db');

var router = express.Router();

router.post('/', function (req, res) {
    var terminoa = req.body.terminoa;
    var cId = req.body.cId;

    sql = 'INSERT INTO Deskribapena(terminoa, cId) VALUES (?, ?)';

    db.query(sql, [terminoa, cId], function (error, results, fields) {
        if (error) throw error;
        res.send(JSON.stringify(results));
    });
});

module.exports = router;
